<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Comment System</title>
   <link href="public/css/main.css"  rel="stylesheet">
   <script src="public/js/jquery.js"></script>
   <script src="public/js/main.js"></script>
</head>
<body>
    <div class ="container">
        <div align="center"><h3>Start a new Conversation</h3></div>
    <form>
    <ul class="form-style-1">
        <li><label>Name <span class="required">*</span></label>
            <input type="text" name="inputname" id="inputname" class="field-divided" placeholder="Name" />&nbsp;
        </li>

        <li>
            <label>Comment <span class="required">*</span></label>
            <textarea name="inputcomment" id="inputcomment" class="field-long field-textarea"></textarea>
        </li>
        <li>
            <input type="button" onclick="validateCommentForm()"value="Submit" />
        </li>
        <li>
            <div class="divider"></div>
        </li>
    </ul>
    </form>
    </div>

    <!-- Comments Div-->

    <div id="comments-container" style="border:1px solid black;">
        <?php if(isset($dynamicHtml)){
                echo $dynamicHtml;
         } //Issset If ends?>
    </div>
</body>
</html>