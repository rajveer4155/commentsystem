<?php
class Commentsmodel extends CI_Model {
	
	function __construct()
	{
            parent::__construct();
            //$this->config->load('common_array');
	}
	public function init()
	{
            $dbHandle = $this->load->database('default', TRUE);
            if($dbHandle == ''){
                error_log('can not create db handle','qna');
                echo (print_r($dbHandle,true));
            }
            return $dbHandle;
	}
	
        
        //*** Getting All comments data to display current comments in system
	public function getAllComments(){
            $dbHandle = $this->init();
            $query = "SELECT * FROM `comments` ORDER BY rootid ASC,created_at ASC";
            $result=$dbHandle->query($query) or die($dbHandle->_error_message());
            $commentsArray = array();
            $i=0;
            $table=[];
            $childrensTable=array();
            foreach ($result->result_array() as $row){
                $parentId=0;

                if($row['rootid']!=0 && $row['replyid']!=0){
                    $parentId=$row['rootid'];
                    $index=$table[$parentId];
                    $size=sizeof($commentsArray[$index]['replies']);
                    $commentsArray[$index]['replies'][$size]=$row;
                    //Saving mapping for comment id and index in array
                    $table[$row['id']]=$i;
                    if(array_key_exists($row['replyid'],$childrensTable)){
                        $childrensTable[$row['replyid']][$row['id']]=$row;
                    }
                    $childrensTable[$row['id']]=[];

                }else{
                    $commentsArray[$i] = $row;
                    $commentsArray[$i]['replies'] = [];
                    $table[$row['id']]=$i;
                }
                $i++;
            }
            $dataArray['commentsArray']=$commentsArray;
            $dataArray['childList']=$childrensTable;
            return $dataArray;
        }
        
        //*** Record root comment
        public function recordRootComment($name,$comment){
            $dbHandle = $this->init();
            //Getting Mysqli for escaping values
            $mysqli=$this->get_mysqli();
            
            $query="INSERT into comments(name,comment,created_at) VALUES('".mysqli_real_escape_string($mysqli,$name)."','".mysqli_real_escape_string($mysqli,$comment)."',NOW());";     
            $result=$dbHandle->query($query) or die($dbHandle->_error_message());
            
            $lastInsertedId=$dbHandle->insert_id();
            $date=$this->getDate($lastInsertedId);
            $formattedDate=date_format(date_create($date), 'jS M Y g:ia');
            $returnString=$lastInsertedId."^".$formattedDate;
            return $returnString;
        }
        
        //*** Record reply comment
        public function recordReplyComment($name,$comment,$rootId,$replyId){
            $dbHandle = $this->init();
            //Getting Mysqli for escaping values
            $mysqli=$this->get_mysqli();
            
            $query="INSERT into comments(rootid,replyid,name,comment,created_at) VALUES('".$rootId."','".$replyId."','".mysqli_real_escape_string($mysqli,$name)."','".mysqli_real_escape_string($mysqli,$comment)."',NOW());";     
            $result=$dbHandle->query($query) or die($dbHandle->_error_message());
            
            $lastInsertedId=$dbHandle->insert_id();
            $date=$this->getDate($lastInsertedId);
            $formattedDate=date_format(date_create($date), 'jS M Y g:ia');
            $returnString=$lastInsertedId."^".$formattedDate;
            return $returnString;
        }
        
        //*** Get Date of Last inserted comment for dynamic date update by JS
        public function getDate($Id){
            $dbHandle = $this->init();
            $query="SELECT created_at FROM comments WHERE id='".$Id."'";     
            $result=$dbHandle->query($query) or die($dbHandle->_error_message());
            $row=$result->result_array();
            return $row[0]['created_at'];
        }
        
        public function get_mysqli() { 
            //$db = (array)get_instance()->db;
            return mysqli_connect('localhost', 'root', '', 'commentsystem');
        }

             
}// EOF 
?>