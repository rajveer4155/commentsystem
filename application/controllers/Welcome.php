<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    
        public $visitedTable; // global HashTable for keeping track of visited nodes
        
	public function index(){

            $this->load->library('session');
	    $this->load->model('commentsmodel');
            $dataArray = $this->commentsmodel->getAllComments();
            //Initializing visited table
            $this->visitedTable=[];
            
            //Start Constructing Dynamic HTML
            $dynamicHTML=$this->constructThreadedHTML($dataArray);
            
            //Sendind to view
            $displayData['dynamicHtml']=$dynamicHTML;
            $this->load->view('comments',$displayData);
	}
        
       
        //*** Function for getting root div HTML dynamically
        public function getRootTemplate($id,$name,$comment,$date){
            
            $formattedDate=date_format(date_create($date), 'jS M Y g:ia');
        
            $str='<div class="root-comment" id="root-'.$id."-0-0".'">'.
                    '<input type="hidden" name="uniqueId-'.$id."-0-0".'" id="uniqueId-'.$id."-0-0".'" value="'.$id."-0-0".'">'.
                    '<input type="hidden" name="rrootid-'.$id."-0-0".'" id="rrootid-'.$id."-0-0".'" value="0">'.
                    '<input type="hidden" name="rId-'.$id."-0-0".'" id="rId-'.$id."-0-0".'" value="'.$id.'">'.
                    '<div style="background:#eee;font-family:sans-serif;" id="comment-disp">'.$comment.
                        '<br> <p style = "font-size:10px">'.$name.'</p>&nbsp; <p style = "font-size:10px">'.$formattedDate.'</p> &nbsp; &nbsp;<a href="javascript:void(0);" data-href-id="'.$id."-0-0".'" class="reply-href">reply</a> </div>
                        <div style="display:none" id=div-comment-reply-'.$id."-0"."-0".'>
                           <a href="Javascript:void(0)" class="close-href-reply" data-reply-close-id="'.$id."-0-0".'">(X)</a>
                            <form id="commentForm-reply-'.$id."-0"."-0".'">
                                <label>Name <span class="required">*</span></label>
                                    <input type="text" name="rname-'.$id."-0"."-0".'" id="rname-'.$id."-0"."-0".'" class="field-divided" placeholder="First" />&nbsp;

                                <label>Comment <span class="required">*</span></label>
                                <textarea name="rcomment-'.$id."-0"."-0".'" id="rcomment-'.$id."-0"."-0".'" class="field-long field-textarea"></textarea>

                                <input type="button" onclick="validateReplyCommentForm(\''.$id."-0-0".'\')" value="Reply" />
                            </form>
                        </div>'; 
                return $str;
        }
        
        
         //*** Function for getting reply div HTML dynamically
        public function getReplyTemplate($id,$rootid,$replyid,$name,$comment,$date,$level){
            $formattedDate=date_format(date_create($date), 'jS M Y g:ia');
            $uniqueid=$rootid."-".$replyid."-".$id;
            $reply="";
            if($level<3){
                $reply='<a href="javascript:void(0);" data-href-id="'.$uniqueid.'" class="reply-href">reply</a>';
            }
            $str='<div class="comment" id="rep-'.$uniqueid.'">'.
                    '<input type="hidden" name="uniqueId-'.$uniqueid.'" id="uniqueId-'.$uniqueid.'" value="'.$uniqueid.'">'.
                    '<input type="hidden" name="rrootid-'.$uniqueid.'" id="rrootid-'.$uniqueid.'" value="'.$rootid.'">'.
                    '<input type="hidden" name="level-'.$uniqueid.'" id="level-'.$uniqueid.'" value="'.$level.'">'.
                    '<input type="hidden" name="rId-'.$uniqueid.'" id="rId-'.$uniqueid.'" value="'.$id.'">'.
                    '<div style="background:#eee;font-family:sans-serif;" id="comment-disp">'.$comment.
                        '<br> <p style = "font-size:10px">'.$name.'</p>&nbsp; <p style = "font-size:10px">'.$formattedDate.'</p> &nbsp; &nbsp;'.$reply.' </div>
                        <div style="display:none" id=div-comment-reply-'.$uniqueid.'>
                           <a href="Javascript:void(0)" class="close-href-reply" data-reply-close-id="'.$uniqueid.'">(X)</a>
                            <form id="commentForm-reply-'.$uniqueid.'">
                                <label>Name <span class="required">*</span></label>
                                    <input type="text" name="rname-'.$uniqueid.'" id="rname-'.$uniqueid.'" class="field-divided" placeholder="First" />&nbsp;

                                <label>Comment <span class="required">*</span></label>
                                <textarea name="rcomment-'.$uniqueid.'" id="rcomment-'.$uniqueid.'" class="field-long field-textarea"></textarea>

                                <input type="button" onclick="validateReplyCommentForm(\''.$uniqueid.'\')" value="Reply" />
                            </form>
                        </div>'; 
                return $str;
        }
        
        //*** Helper function for thread recursion
         public function addThread($id,$reply,$childArray,$level){
            $str="";
            $str.=$this->getReplyTemplate($reply['id'], $reply['rootid'], $reply['replyid'], $reply['name'], $reply['comment'], $reply['created_at'],$level);
            if($this->visitedTable[$id]==false){
                foreach ($childArray[$reply['id']] as $childKey => $child){
                    $str.=$this->addThread($childKey,$child,$childArray,++$level);
                    $level--;
                }
            }
            $str.="</div>";
            $this->visitedTable[$id]=true;
            return $str;
         }
         
        public function constructThreadedHTML($dataArray){
            
            //Getting comments array
            $commentsArray=$dataArray['commentsArray'];
            
            //Getting Adjacency List of Children
            $childArray=$dataArray['childList'];
           
            //Initializing each entry of visitor table to false
            foreach($childArray as $key => $value){
                $this->visitedTable[$key]=false;
            }
            
            //Variable to keep whole dynamic html
            $mainString='';
            //Starting Main LOOP
            foreach($commentsArray as $comment){
                $mainString.=$this->getRootTemplate($comment['id'],$comment['name'],$comment['comment'],$comment['created_at']);
                    $level=1;
                    foreach($comment['replies'] as $reply){
                        if($this->visitedTable[$reply['id']]==false){
                            $mainString.=$this->addThread($reply['id'],$reply,$childArray,$level);
                         }
                    } //Inner for each ends
                $mainString.='</div>';
            } //outer for-each ends
            return $mainString;
        }
        
        
        //*** Record New Conversation comment(Root Comment)
        public function recordCommentForm(){
            $this->load->model('commentsmodel');
            $name=$_POST['inputname'];
            $comment=$_POST['inputcomment'];
            $recordedRootId=$this->commentsmodel->recordRootComment($name,$comment);
            echo $recordedRootId;
        }
        
        //*** Record Reply comment(Reply Comment)
         public function recordReplyCommentForm(){
            $this->load->model('commentsmodel');
            $name=$_POST['inputname'];
            $comment=$_POST['inputcomment'];
            $rootId=$_POST['rootid'];
            $replyId=$_POST['replyid'];
            $recordedRootId=$this->commentsmodel->recordReplyComment($name,$comment,$rootId,$replyId);
            echo $recordedRootId;
        }
}
