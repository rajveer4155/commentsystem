//*** Submitting Reply Form
function validateCommentForm(replyId){
    var name=document.getElementById('inputname').value;
    var comment=document.getElementById('inputcomment').value;
    if (name==='') {
        alert("Error! Please enter your name");
        document.getElementById('inputname').value = '';
        document.getElementById('inputname').focus();
        return false;

    }
    if(comment=== ""){ 
        alert("Error! Please Enter Comment"); 
        document.getElementById('inputcomment').value = '';
        document.getElementById('inputcomment').focus(); 
        return false; 
    }
    var url = "/recordCommentForm"; 
    var datastring  = 'inputname='+name+'&inputcomment='+comment; 
    $.ajax({ 
        type   : "POST",  
        url    : url,  
        data   : datastring,        
        success: function(responseText) { 
            var result=responseText.split('^');
            var insertedId=result[0];
            var date=result[1];
            var str='<div class="root-comment" id="root-'+insertedId+"-0-0"+'">'+
                    '<div style="background:#eee;font-family:sans-serif;" id="comment-disp">'+comment+
                    '<br> <p style = "font-size:10px">'+name+'</p>&nbsp; <p style = "font-size:10px">'+date+'</p> &nbsp; &nbsp;'+
                    '<a href="javascript:void(0)" data-href-id="'+insertedId+"-0-0"+'" class="reply-href" >reply</a> </div>'+
                    '<div style="display:none" id=div-comment-reply-'+insertedId+"-0"+"-0"+'>'+
                    '<a href="javascript:void(0)" class="close-href-reply" data-reply-close-id="'+insertedId+'-0-0'+'">(X)</a'+
                    '<form id="commentForm-reply-'+insertedId+"-0"+"-0"+'">'+
                    '<label>Name <span class="required">*</span></label>'+
                    '<input type="text" name="rname-'+insertedId+"-0"+"-0"+'" id="rname-'+insertedId+"-0"+"-0"+'"'+ 						'class="field-divided" placeholder="First" />&nbsp;'+
                    '<label>Comment <span class="required">*</span></label>'+
                    '<textarea name="rcomment-'+insertedId+"-0"+"-0"+'" id="rcomment-'+insertedId+"-0"+"-0"+
                    '" class="field-long field-textarea"></textarea>'+
                    '<input type="button" onclick="validateCommentForm(\''+insertedId+"-0"+"-0"+'\')" value="Reply" /></form></div></div>';
                    
            //*** Appending dynamic div to its parent 
            var maindiv = document.getElementById('comments-container');
            maindiv.innerHTML = maindiv.innerHTML + str;
            alert("New Conversation started successfully!");
            document.getElementById('inputname').value="";
            document.getElementById('inputcomment').value="";
        }

    });
}

//*** Submitting Reply Form

function validateReplyCommentForm(uniqueId){

    var name=document.getElementById('rname-'+uniqueId).value;
    var comment=document.getElementById('rcomment-'+uniqueId).value;
    var rootid=document.getElementById('rrootid-'+uniqueId).value;
    var id=document.getElementById('rId-'+uniqueId).value; 
    var level=0;
    if (name==='') {
        alert("Error! Please enter your name");
        document.getElementById('rname-'+uniqueId).value = '';
        document.getElementById('rname-'+uniqueId).focus();
        return false;

    }
    if(comment=== ""){ 
        alert("Error! Please Enter Comment"); 
        document.getElementById('rcomment-'+uniqueId).value = '';
        document.getElementById('rcomment-'+uniqueId).focus(); 
        return false; 
    }
    var sendrootId=rootid
    if(rootid==0){ //if root, set rootid equal to reply id to signify first child
         sendrootId=id;
     }else{ //if not root; get level 
         level=document.getElementById('level-'+uniqueId).value;
     }
    var url = "/recordReplyCommentForm"; 
    var datastring  = 'inputname='+name+'&inputcomment='+comment+'&rootid='+sendrootId+'&replyid='+id; 
    $.ajax({ 
        type   : "POST",  
        url    : url,  
        data   : datastring,        
        success: function(responseText) { 
            var result=responseText.split('^');
            var insertedId=result[0];
            var date=result[1];
            var newuniqueId=rootid+"-"+id+"-"+insertedId;
            var reply='';
            if(level<2){
                reply='<a href="javascript:void(0)" data-href-id="'+newuniqueId+'" class="reply-href" >reply</a>';
            }
            var str='<div class="comment" id="rep-'+newuniqueId+'">'+
                    '<div style="background:#eee;font-family:sans-serif;" id="comment-disp">'+comment+
                    '<br> <p style = "font-size:10px">'+name+'</p>&nbsp; <p style = "font-size:10px">'+date+'</p> &nbsp; &nbsp;'+reply+'</div>'+
                    '<div style="display:none" id=div-comment-reply-'+newuniqueId+'>'+
                    '<a href="javascript:void(0)" class="close-href-reply" data-reply-close-id="'+newuniqueId+'">(X)</a'+
                    '<form id="commentForm-reply-'+newuniqueId+'">'+
                    '<label>Name <span class="required">*</span></label>'+
                    '<input type="text" name="rname-'+newuniqueId+'" id="rname-'+newuniqueId+'"'+ 						'class="field-divided" placeholder="First" />&nbsp;'+
                    '<label>Comment <span class="required">*</span></label>'+
                    '<textarea name="rcomment-'+newuniqueId+'" id="rcomment-'+newuniqueId+
                    '" class="field-long field-textarea"></textarea>'+
                    '<input type="button" onclick="validateCommentForm(\''+newuniqueId+'\')" value="Reply" /></form></div></div>';
             
            var prefix="";
            if(rootid==0 ){
                    prefix="root-";
            }else{
                 prefix="rep-";
            }
            //*** Appending dynamic div to its parent 
            var maindiv = document.getElementById(prefix+uniqueId);
            maindiv.innerHTML = maindiv.innerHTML + str;
            document.getElementById('div-comment-reply-'+uniqueId).style.display = "none";
            
            alert("Reply posted successfully!");
            document.getElementById('rname-'+uniqueId).value="";
            document.getElementById('rcomment-'+uniqueId).value="";
        }

    });
}
    
    
//*** Open reply form
function openReplyForm(event){
    replyId=$(event.currentTarget).data("href-id");
    if(replyId!=""){
        document.getElementById('div-comment-reply-'+replyId).style.display = "block";
    }
}

//***close reply form Id
function closeReplyForm(event){
    replyId=$(event.currentTarget).data("reply-close-id");
    if(replyId!=""){
        document.getElementById('div-comment-reply-'+replyId).style.display = "none";
    }
}

//**** For binding click events to Dynamically added threads
$(function () {
    $("#comments-container").on("click", ".reply-href", openReplyForm);
    $("#comments-container").on("click", ".close-href-reply", closeReplyForm);
});